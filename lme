#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Sync your ledger files with Android App MyExpenses
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/ledgermyexpenses/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

_log() {
  # Log to stderr. Some functions use stdout to return data.
  echo "$1" 1>&2
}

_parseenv() {
  # Stop if we don't have the right env/vars set
  if [[ "$LME_SYNC_DIR" == "" ]]; then
    _log "LME_SYNC_DIR has not been set"
    exit 1
  fi
  if [[ "$LME_ACCOUNT_LABEL" == "" ]]; then
    _log "LME_ACCOUNT_LABEL has not been set"
    exit 1
fi
}

_findaccountuuidbylabel() {
  for metadata in "$LME_SYNC_DIR"/*/metadata.json; do
    _label=$(jq -r '.label' < "$metadata" ) || exit 1
    if [[ "$_label" == "$LME_ACCOUNT_LABEL" ]]; then
      jq -r '.uuid' < "$metadata" 
      return 0
    fi
  done
  return 1
}

_getopeningbalance() {
  # TODO: We could warn user if these vars are not specified
  cmdtorun="$LME_LEDGER_CMD_OPENING_BAL"
  cmdtorun+=" -e $LME_LEDGER_START_DATE"

  _log "Retrieving opening balance with command:"
  _log "$cmdtorun"
  cmdout=$(eval "$cmdtorun") || exit 1

  # Read last line
  cmdout=$(echo "$cmdout" | tail -n 1)
  balance=$(printf '%.0f\n' "$(bc <<< "$cmdout * 100")")

  if ! [[ "$balance" =~ ^[0-9]+$ ]]; then
    _log "Opening balance was not a number: $balance"
    exit 1
  else
  _log "Opening balance found: $balance"
  fi

  LME_OPENING_BALANCE="$balance"
}

_createaccount() {
  uuid=$(uuidgen) || exit 1
  # create the dir
  newdir="$LME_SYNC_DIR/$uuid"
  mkdir "$newdir"
  read -r outer << EOF
{"label":"$LME_ACCOUNT_LABEL","currency":"GBP","color":-16738680,"uuid":"$uuid","openingBalance":$LME_OPENING_BALANCE,"description":"","type":"CASH","excludeFromTotals":false,"criterion":0}
EOF
  echo "$outer" > "$newdir"/metadata.json
}

_lockbackend() {
  if [ -f "$LME_SYNC_DIR/$lockfile" ]; then
    _log "Backend already locked"
    return 1
  else
    declare -g lock_uuid
    lock_uuid=$(uuidgen) || exit 1
    echo "$lock_uuid" > "$LME_SYNC_DIR/$lockfile"
    _log "Backend locked with uuid: $lock_uuid"
  fi
}

_unlockbackend() {
  if [ -f "$LME_SYNC_DIR/$lockfile" ]; then
    found_lock=$(< "$LME_SYNC_DIR/$lockfile") || exit 1
    if [[ "$found_lock" == "$lock_uuid" ]]; then
      rm -f "$LME_SYNC_DIR/$lockfile" || exit 1
      _log "Successfully removed our lock file"
    else
      _log "Lock file found but lock uuid does not match ours"
      return 1
    fi
  fi
}

_initnextchangesetfilename() {
  # Set global vars changesetfilename and shard based on existing files in backend

  declare -g changesetfilename=""
  declare -g shard=""

  # https://stackoverflow.com/a/34195247
  if ! compgen -G "$LME_SYNC_DIR/$uuid/_*.json" > /dev/null; then
    _log "No changeset files found, starting from _1.json"
    changesetfilename="_1.json"
  else
    # Are there any shard dirs?
    if compgen -G "$LME_SYNC_DIR/$uuid/_*/*.json" > /dev/null; then
      # if so, find the biggest and set that as our base
      shard=$(find "$LME_SYNC_DIR/$uuid" -type d -name "_*" -printf "%f\n" \
                          | sort -V | tail -n 1)
      _log "Shard(s) found, current is $shard"
    fi
    changesetfilename=$(find "$LME_SYNC_DIR/$uuid/$shard" -type f -name "_*.json" -printf "%f\n" \
                        | sort -V | tail -n 1)
    _setnextchangesetfilename
  fi
}

_setnextchangesetfilename() {
  # Set global vars changesetfilename and shard

  # Hokey way to get the number out the filename
  IFS=_. read -r _ _lastchangesetnumber _ < <(basename "$changesetfilename")

  if ! [[ "$_lastchangesetnumber" =~ ^[0-9]+$ ]]; then
    _log "Last changeset number was not a number: $_lastchangesetnumber"
    exit 1
  else
    _log "Last changeset number: $_lastchangesetnumber"
  fi

  _lastchangesetnumber=$((_lastchangesetnumber + 1))

  # This limit hardcoded in MyExpenses codebase
  # https://github.com/mtotschnig/MyExpenses/blob/master/myExpenses/src/main/java/org/totschnig/myexpenses/sync/SequenceNumber.kt
  if [[ "$_lastchangesetnumber" -ge 100 ]]; then
    _lastchangesetnumber=1
    if [[ "$shard" == "" ]]; then
      shard="_1"
    else
      shard=${shard#_}
      shard="_$((shard + 1))"
    fi
  fi

  changesetfilename="$shard/_$_lastchangesetnumber.json"
  _log "Next changesetfilename: $changesetfilename"
}

_writechangeset() {
  local _inputdate=$1;shift
  local _inputamount=$1;shift
  local _inputpayee=$1;shift
  local _inputaccount=$1;shift
  local _inputcomment=$1;shift

  mkdir -p "$(dirname "$LME_SYNC_DIR/$uuid/$changesetfilename")" || exit 1

  local _transactionuuid=$nexttransactionuuid
  local _timestamp;_timestamp=$(date +%s) || exit 1

  # Value date, convert to epoch
  local _valuedate;_valuedate=$(date -d "$_inputdate" +%s)
  local _date=$_valuedate
  # Amount, convert to 00s, chop off any extra decimals
  local _amount;_amount=$(printf '%.0f\n' "$(bc <<< "$_inputamount * 100")")
  # Escape payee for JSON, just deal with \ and " for now
  local _payee=${_inputpayee/\\/\\\\}
  _payee=${_payee/\"/\\\"}
  _getcatjsonforledgeracc "$_inputaccount" || exit 1
  local _comment
  # Comment - strip any preceeding effective date which ledger treats as comment
  if [[ "$_inputcomment" =~ ^[[:blank:]]*\[=..../../..\][[:blank:]]*(.*) ]]; then
    _inputcomment=${BASH_REMATCH[1]}
  fi
  # Comment - take last line only, trim any preceeding whitespace
  if [[ "$_inputcomment" =~ .*\\n[[:blank:]]*(.*) ]]; then
    _comment=${BASH_REMATCH[1]}
  else
    _comment="$_inputcomment"
  fi

  local _changedata
  read -r _changedata << EOF
[{"type":"created","uuid":"$_transactionuuid","timeStamp":$_timestamp,"date":$_date,"valueDate":$_valuedate,"amount":$_amount,"payeeName":"$_payee","crStatus":"UNRECONCILED","comment":"$_comment","categoryInfo":$catjson}]
EOF
  echo "$_changedata" > "$LME_SYNC_DIR/$uuid/$changesetfilename"
}

_getledgertransactions() {
  # TODO: We could warn user if these vars are not specified
  _cmdtorun="$LME_LEDGER_CMD_TRANSACTIONS"
  # Set date limits for ledger
  _cmdtorun+=" -b $LME_LEDGER_START_DATE"
  _cmdtorun+=" -e $(date --date 'tomorrow' +%Y/%m/%d)"

  _log "Retrieving ledger transactions with command:"
  _log "$_cmdtorun"
  _cmdout=$(eval "$_cmdtorun") || exit 1
  echo "$_cmdout"
}

_loadledgeracctomecatmappings() {
  declare -Ag mappings
  # TODO: Use XDG standard
  _mappingconfig="$HOME/.config/ledgermyexpenses/mappings"

  if [ -f  "$_mappingconfig" ]; then
    while IFS=\| read -r _acc _treeid; do
      # Skip comment lines
      if [[ $_acc =~ ^#.* ]]; then
        continue
      fi
      # TODO: Verify sensible input
      mappings["$_acc"]="$_treeid"
    done < "$_mappingconfig"
  else
    _log "No mapping config found: $_mappingconfig"
  fi
}

_loadmecattoledgeraccmappings() {
  declare -Ag mappingsledger
  # TODO: Use XDG standard
  _mappingconfig="$HOME/.config/ledgermyexpenses/mappings_ledger"

  if [ -f  "$_mappingconfig" ]; then
    while IFS=\| read -r _treeid _acc1 _acc2; do
      # Skip comment lines
      if [[ $_acc =~ ^#.* ]]; then
        continue
      fi
      # TODO: Verify sensible input
      mappingsledger["$_treeid"]="$_acc1,$_acc2"
    done < "$_mappingconfig"
  else
    _log "No mapping ledger config found: $_mappingconfig"
  fi
}

_getcatjsonforledgeracc() {
  local _ledgeracc=$1;shift

  if [[ -v mappings["$_ledgeracc"] ]]; then
    _treeid="${mappings["$_ledgeracc"]}"
  else
    _log "No mapping found for ledger account: $_ledgeracc"
    exit 1
  fi

  _getcatjsonfortreeid "$_treeid"
}

_getcatjsonfortreeid() {
  # We return the json in global catjson rather than stdout as this function
  # needs to modify this env's mecategories so cannot be run in a subshell.
  declare -g catjson=""
  local _treeid=$1;shift

  if [[ "$_treeid" == "NULL" ]]; then
    # No category
    catjson="[]"
    _log "Category json: $catjson"
    return
  fi

  while true; do
    if [[ ! -v mecategories["$_treeid"] ]]; then
      _log "No existing category uuid for treeid [$_treeid], creating new"
      mecategories["$_treeid"]=$(uuidgen) || exit 1
    fi
    if [[ ! -v caticons["$_treeid"] ]]; then
      _log "No category config for treeid [$_treeid]"
      exit 1
    fi
    _icon="${caticons["$_treeid"]}"
    _label=${_treeid%%,*} # Label is first field, comma-sep
    _catuuid="${mecategories["$_treeid"]}"

    catjson="{ \"icon\": \"$_icon\", \"label\": \"$_label\", \"uuid\": \"$_catuuid\" }, $catjson"

    # Are there still category parents to deal with?
    if [[ "$_treeid" =~ , ]]; then
      _treeid=${_treeid#*,} # next we look at parent of this cat
    else
      break
    fi
  done

  catjson="[ ${catjson%%, } ]" # trim the end and put in json array
  _log "Category json: $catjson"
}

_loadcats() {
  # TODO: We should probably care about the order we read these and let newer
  # categories override older.
  # TODO: MyExpenses ignores categories of deleted transactions. We should do the same.
  
  declare -Ag mecategories
  _log "Reading existing categories from backend"

  while IFS= read -r -d '' file; do
    _loadcat "$file"
  done < <(find "$LME_SYNC_DIR/$uuid" -type f -name "_*.json" -print0)
}

_loadcat() {
  _file=$1;shift

  # use jq to extract the category tree from file
  # TODO: We do this is a nicer way with jq below with reverse and join so we
  # don't need to compose treeid ourselves.
  while read -r _line; do
    case "$_line" in
      # Start of new cat tree
      \[ )
        _treeid=""
        ;;
      # End of category
      *\} | *\}, )
        _log "Adding treeid: [$_treeid] with catuuid: $_catuuid"
        # TODO: look it up first, is it there with a different uuid? What would that mean?
        mecategories["$_treeid"]="$_catuuid"

        # Reset them for next
        _label=""
        _catuuid=""
        ;;
      *\"label\":* )
        if [[ $_line =~ .*:\ \"(.*)\" ]]; then
          _label=${BASH_REMATCH[1]}
        fi

        # Build up treeid with children
        if [[ "$_treeid" == "" ]]; then
          _treeid="$_label"
        else
          _treeid="$_label,$_treeid"
        fi
        ;;
      *\"uuid\":* )
        if [[ $_line =~ .*:\ \"(.*)\" ]]; then
          _catuuid=${BASH_REMATCH[1]}
        fi
        ;;
    esac
  done < <(jq -r '.[].categoryInfo' < "$file")
}

_addmissingcats() {
  local _treeid
  for _treeid in "${!caticons[@]}"; do
    if [[ ! -v mecategories["$_treeid"] ]]; then
      _log "We got a category missing from the backend: $_treeid"

      local _transactionuuid;_transactionuuid=$(uuidgen) || exit 1
      local _timestamp;_timestamp=$(date +%s) || exit 1
      local _valuedate;_valuedate=$(date -d "2000/01/01" +%s)
      local _date=$_valuedate
      local _amount;_amount="99"
      local _payee="ledgermyexpenses"
      _getcatjsonfortreeid "$_treeid"
      read -r _changedata << EOF
[{"type":"created","uuid":"$_transactionuuid","timeStamp":$_timestamp,"date":$_date,"valueDate":$_valuedate,"amount":$_amount,"payeeName":"$_payee","crStatus":"VOID","categoryInfo":$catjson}]
EOF
      echo "$_changedata" > "$LME_SYNC_DIR/$uuid/$changesetfilename"
      _setnextchangesetfilename
    fi
  done
}

_loadcaticons() {
  # Set global caticons
  declare -Ag caticons
  # TODO: Use XDG standard
  _categoryconfig="$HOME/.config/ledgermyexpenses/categories"

  if [ -f  "$_categoryconfig" ]; then
    while IFS=\| read -r _treeid _icon; do
      # Skip comment lines
      if [[ $_treeid =~ ^#.* ]]; then
        continue
      fi
      # TODO: Verify sensible input
      caticons["$_treeid"]="$_icon"
      _log "Added icon [$_icon] for category [$_treeid]"
    done < "$_categoryconfig"
  else
    _log "No category config found: $_categoryconfig"
  fi
}

_loadconfig() {
  # TODO: Use XDG standard
  _config="$HOME/.config/ledgermyexpenses/config"

  # shellcheck disable=SC1090
  . "$_config" || exit 1
}

_transactionwrittenbefore() {
  _inputdate=$1;shift
  _inputamount=$1;shift
  _inputpayee=$1;shift

  if [[ ! -v newtransactionlog ]]; then
    declare -Ag newtransactionlog
  fi
  if [[ ! -v nexttransactionuuid ]]; then
    declare -g nexttransactionuuid 
  fi

  _hash="$_inputdate$_inputpayee$_inputamount#0"

  # Have we seen it before on THIS run?
  # which suggest a trans has been repeated on same day, so up the index
  declare -i i=0
  while [[ -v newtransactionlog["$_hash"] ]];do
    i+=1
    _hash="$_inputdate$_inputpayee$_inputamount#$i"
  done

  if [[ ! -v transactionlog["$_hash"] ]]; then
    _log "We've NOT seen this transaction before: $_hash"
    nexttransactionuuid=$(uuidgen) || exit 1
    newtransactionlog["$_hash"]="$nexttransactionuuid"
    return 1
  else
    _log "We've seen this transaction before: $_hash"
    nexttransactionuuid="${transactionlog["$_hash"]}"
    newtransactionlog["$_hash"]="$nexttransactionuuid"
    return 0
  fi
}

_checktransactionforupdate() {
  local _inputdate=$1;shift
  local _inputamount=$1;shift
  local _inputpayee=$1;shift
  local _inputfilename=$1;shift
  local _inputlineno=$1;shift

  # TODO: We only ever check #0, so will only change one entry of duplicates
  local _hash="$_inputdate$_inputpayee$_inputamount#0"

  # Get the uuid of this hash
  if [[ ! -v newtransactionlog["$_hash"] ]]; then
    _log "Somehow we have no uuid in the transactionlog for hash: $_hash"
    exit 1
  fi
  local _uuid="${newtransactionlog["$_hash"]}"

  if [[ -v uuidupdate["$_uuid"] ]]; then
    _log "== We need to update ledger for: $_inputdate $_inputpayee $_inputamount"
    _log "== $_inputfilename at $_inputlineno"
    if [[ ! -v ledgerchanges ]]; then
      declare -Ag ledgerchanges
    fi
    ledgerchanges["$_inputfilename"]+="$_uuid,$_inputlineno|"
  fi
}

_savetransactionlog() {
  _translogfile="$HOME/.local/share/ledgermyexpenses/transactionlog"
  mkdir -p "$(dirname "$_translogfile")" || exit 1

  for _transaction in "${!newtransactionlog[@]}";do
    _transactionlog+="${newtransactionlog["$_transaction"]}|$_transaction\n"
  done

  echo -e "${_transactionlog%%\\n}" > "$_translogfile"
}

_loadtransactionlog() {
  _translogfile="$HOME/.local/share/ledgermyexpenses/transactionlog"
  declare -Ag transactionlog
  test -f "$_translogfile" || return

  while IFS='|' read -r _uuid _transaction; do
    _log "Adding transaction to uuid log with uuid: $_uuid, transaction: $_transaction"
    transactionlog["$_transaction"]="$_uuid"
  done < "$_translogfile"
}

_loadupdatesfrombackend() {
  declare -gA uuidupdate uuidupdate_comment uuidupdate_category
  local _sharddir _file _line _shards _result

  # Get the shards in the right order
  _shards=$(find "$LME_SYNC_DIR/$uuid" -type d "(" -name "_*" -o -name "$uuid" ")" -and -printf "%p\n" | sort -V) || exit 1

  while IFS= read -r -d $'\n' _sharddir; do
    while IFS= read -r -d '' _file; do
      _result=$(jq '.[] | select(.type == "updated") |
          { "uuid": .uuid } +
          if .comment then
            { "comment": .comment } 
          else 
            {} 
          end 
          +
          if .categoryInfo then 
            { "categoryInfo": ([.categoryInfo[].label] | reverse | join(",") ) }
          else 
            {} 
          end' < "$_file") || exit 1

      if [[ "$_result" != "" ]]; then
        _log "Update to process in $_file"
        _log "$_result"
        while IFS= read -r -d $'\n' _line; do
          if [[ $_line =~ .*uuid\":\ \"(.*)\" ]]; then
            _uuid=${BASH_REMATCH[1]}
          elif [[ $_line =~ .*categoryInfo\":\ \"(.*)\" ]]; then
            _log "Category update for $_uuid: ${BASH_REMATCH[1]}"
            uuidupdate["$_uuid"]=1
            uuidupdate_category["$_uuid"]=${BASH_REMATCH[1]}
          elif [[ $_line =~ .*comment\":\ \"(.*)\" ]]; then
            _log "Comment update for $_uuid: ${BASH_REMATCH[1]}"
            uuidupdate["$_uuid"]=1
            uuidupdate_comment["$_uuid"]=${BASH_REMATCH[1]}
          fi
        done < <(echo -e "$_result")
      fi
    done < <(find "$_sharddir" -maxdepth 1 -type f -name "_*.json" -print0 | sort -V)
  done < <(echo -e "$_shards")
}

_applyledgerupdates() {
  local _filename _uuid _lineno _pair _offsetlineno
  declare -ig updateledgerfilelineoffset

  for _filename in "${!ledgerchanges[@]}"; do
    _log "Updates for $_filename"
    updateledgerfilelineoffset=0
    while IFS=',' read -r -d '|' _uuid _lineno; do
      _offsetlineno=$((_lineno + updateledgerfilelineoffset))
      _log "  Changes for MyExpenses transaction uuid: $_uuid, linked to line no $_lineno"

      if [[ -v uuidupdate_comment["$_uuid"] ]]; then
        _updateledgercomment "$_filename" "$_offsetlineno" "${uuidupdate_comment["$_uuid"]}"
      fi
      _offsetlineno=$((_lineno + updateledgerfilelineoffset))
      if [[ -v uuidupdate_category["$_uuid"] ]]; then
        _updateledgercategory "$_filename" "$_offsetlineno" "${uuidupdate_category["$_uuid"]}"
      fi
    done < <(echo "${ledgerchanges["$_filename"]}")
  done
}

_updateledgercomment() {
  local _filename _lineno _comment _commentlineno _commentline _existing
  _filename=$1;shift
  _lineno=$1;shift
  _comment=$1;shift

  _commentlineno=$(( _lineno - 1 ))
  _commentline="    ; $_comment"

  _existing=$(sed "${_commentlineno}!d" "$_filename")

  _log "    Comment should be: $_comment on line $_lineno"
  if [[ "$_existing" != "$_commentline" ]];then
    _log "      Comment missing, will add."
    sed -i  "$_filename" -e "${_lineno}i\\" -e "$_commentline"
    updateledgerfilelineoffset+=1
  else
    _log "      Comment found. No change needed."
  fi
}

_updateledgercategory() {
  local _filename _lineno _category _existingline1 _existingline2 _acc1 _acc2
  _filename=$1;shift
  _lineno=$1;shift
  _category=$1;shift

  IFS=, read -r _acc1 _acc2 <<< "${mappingsledger["$_category"]}"

  _existingline1=$(sed "${_lineno}!d" "$_filename")
  _existingline2=$(sed "$(( _lineno + 1))!d" "$_filename")

  _log "    Category update: MyExpenses category [$_category] should be translated to: $_acc1 and $_acc2 on line no $_lineno"
  _log "      Got line1: $_existingline1"
  _log "      Got line2: $_existingline2"

  if [[ "$_acc1" == "" || "$_acc2" == "" ]]; then
    _log "       WARNING: no mapping found, so no action will be taken."
    return
  fi

  # Understand ledger format here
  # [The Most Basic Entry](https://ledger-cli.org/doc/ledger3.html#The-Most-Basic-Entry)
  # I don't believe regex alone can parse all valid ledger postings
  # Here we have to restrict account names: not allowing spaces for our matches to work
  # Otherwise we cannot distinguish ledger's 'hard separator' from an account name
  if [[ ! "$_existingline1" =~ ^[[:space:]]+$_acc1(([[:space:]]+|\t)|$) ]];then
    _log "    line1 needs to change"
    sed -i  "$_filename" -e "${_lineno}s/\([[:space:]]\+\)\([a-zA-Z0-9:]\+\)\(  \|\\t\)\?/\\1$_acc1\\3/"
  else
    _log "      line1 ok"
  fi
  if [[ ! "$_existingline2" =~ ^[[:space:]]+$_acc2(([[:space:]]+|\t)|$) ]];then
    _log "    line2 needs to change"
    sed -i  "$_filename" -e "$(( _lineno + 1))s/\([[:space:]]\+\)\([a-zA-Z0-9:]\+\)\(  \|\\t\)\?/\\1$_acc2\\3/"
  else
    _log "      line2 ok"
  fi
}

# ------------------------------------------------------------------------------
# MAIN
# ------------------------------------------------------------------------------

lockfile=".lock.txt"

# Naming conventions:
# Our functions are called _name to distinguish from cmds
# Variables:
#   ENV_VAR - expected to be set in environment
#   name - a global within this script
#   _name - a local within a function

_loadconfig
_parseenv
_loadledgeracctomecatmappings
_loadmecattoledgeraccmappings
_loadcaticons
_loadtransactionlog

_lockbackend || exit 1

if uuid=$(_findaccountuuidbylabel); then
  _log "Found an account with label: $LME_ACCOUNT_LABEL"
  _log "UUID: $uuid"
else
  _log "DID NOT find an account with label: $LME_ACCOUNT_LABEL"
  _getopeningbalance || exit 1
  _createaccount || exit 1
  _log "New account created with uuid: $uuid"
fi

_loadcats
_loadupdatesfrombackend
_initnextchangesetfilename
_addmissingcats

while IFS=\| read -r _filename _lineno _date _amount _payee _account _comment _; do
  # TODO: check we did read something sensible
  if ! _transactionwrittenbefore "$_date" "$_amount" "$_payee"; then
    _writechangeset "$_date" "$_amount" "$_payee" "$_account" "$_comment"
    _setnextchangesetfilename
  fi

  _checktransactionforupdate "$_date" "$_amount" "$_payee" "$_filename" "$_lineno"
done <<< "$(_getledgertransactions)"

_unlockbackend
_applyledgerupdates
_savetransactionlog
