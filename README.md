<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

Sync your ledger files with Android App MyExpenses

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ledgermyexpenses/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# LedgerMyExpenses

Sync your ledger files with the Android App [My
Expenses](https://www.myexpenses.mobi/en/).

**NOTE** LedgerMyExpenses is currently in early development and should be used with
caution. It will can change both your ledger files and your accounts in
MyExpenses so ensure both are **backed up**.

Features:

* **Syncs transactions** between ledger and MyExpenses, from any start date you want
  to the present day.
* **Understands MyExpenses' categories and ledger's accounts**, converting
  from one to the other when syncing. For example picking 'Groceries' in the
  MyExpenses app could be translated to a double-entry of `Assets:Checking` and
  `Expenses:Groceries` in ledger.
* Fully **customisable categories** for MyExpenses - pick your own labels and
  icons for each category.
* **Adapts to your ledger setup**. Define the exact ledger cmdline to use for
  retrieving the opening balance and transaction listings.
* Written in BASH and aimed at Linux users but let me know if support for other
  platforms is desirable.

Current restrictions:

* We can only sync with one account in MyExpenses for now.
* There's no way to manage currency yet, it uses GBP a default. Easy to fix
  though.
* We don't support encrypted MyExpenses accounts/sync (yet).
* The sync is quite basic at the moment, it will improve soon:
  * If a transaction in ledger changes its date/payee/amount its seen as
    something new, rather than a change. We'll will address this in future.
  * Similarly new transactions in MyExpenses are not synced back to ledger yet,
    only changes to existing ones.
  * ...only changes to categories and comments are synced from MyExpenses back
    to ledger currently.
* It's not that efficient either yet - takes a couple of seconds for a few
  hundred transactions, it could be a lot faster. I don't think that's a big
  deal yet though and there's lots of easy ways to improve it.
* Little/no sanity checking of configuration and other inputs/output. Also
  recovering from errors is not graceful.
* Verbose output when it runs.

## Tables of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
  * [Prerequisites](#prerequisites)
  * [Git Clone](#git-clone)
* [Usage](#usage)
  * [Configuration of LedgerMyExpenses](#configuration-of-ledgermyexpenses)
    * [1. Main Config](#1-main-config)
    * [2. Categories](#2-categories)
    * [3. Mappings](#3-mappings)
    * [4. Mappings - Ledger](#4-mappings-ledger)
  * [Configuration of MyExpenses](#configuration-of-myexpenses)
  * [Regular Syncing](#regular-syncing)
* [Troubleshooting](#troubleshooting)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

[MyExpenses](https://www.myexpenses.mobi/) is one of the best apps for managing
finances on Android but I want to keep using [ledger](https://ledger-cli.org/)
and [plain text accounting](https://plaintextaccounting.org/) as well. This tool
allows ledger and MyExpenses to talk to each other using the synchronization
feature of MyExpenses.

## Install

### Prerequisites

* [ledger](https://ledger-cli.org/)
* [jq](https://jqlang.github.io/jq/)
* [BASH](https://www.gnu.org/software/bash/) - probably 4.0+.
* `uuidgen` - this may be part of your distro already, or not. Under Debian you
  have to install `uuid-runtime` elsewhere it may be part of `util-linux`.

The synchronization feature of MyExpenses can be used for a trial period but
ultimately you'll need to [purchase](https://www.myexpenses.mobi/en/#premium) an
extended or professional key.

### Git Clone

For now please just clone the
[repo](https://gitlab.com/IanTwenty/ledgermyexpenses) locally and run the code
directly from there:

```bash
# SSH
git clone git@gitlab.com:IanTwenty/ledgermyexpenses.git
# HTTPS
git clone https://gitlab.com/IanTwenty/ledgermyexpenses.git
```

The actual script is simply called `lme` and we may refer to it as that in
the rest of these instructions. Don't run it yet though, there's some
configuration to complete first.

**Interested in a Docker/Podman/container version? Let me know.**

## Usage

There's a fair bit of setup to do before we can get started:

* Configuration of LedgerMyExpenses
* Configuration of MyExpenses

We go over this in the following subsections.

### Configuration of LedgerMyExpenses

We need to setup a config dir and some config files, in BASH:

```bash
mkdir -p $HOME/.config/ledgermyexpenses
touch $HOME/.config/ledgermyexpenses/config
touch $HOME/.config/ledgermyexpenses/categories
touch $HOME/.config/ledgermyexpenses/mappings
touch $HOME/.config/ledgermyexpenses/mappings_ledger
```

#### 1. Main Config

We'll tackle the main config first: `$HOME/.config/ledgermyexpenses/config`. The
following BASH will populate it with some initial **example** settings:

```bash
cat > $HOME/.config/ledgermyexpenses/config << EOF
LME_SYNC_DIR="$HOME/MyExpenses"
LME_ACCOUNT_LABEL="Checking"
LME_LEDGER_START_DATE="2023/06/01"
LME_LEDGER_CMD_OPENING_BAL="\
  ledger -f $HOME/ledger/main.ldg reg \
  --effective ^Assets --format \"%(quantity(display_total))\n\""
LME_LEDGER_CMD_TRANSACTIONS="\
  ledger -f $HOME/ledger/main.ldg reg \
  ^Assets --effective \
  --format \"%(filename)|%(beg_line)|%(date)|%(quantity(display_amount))|%(payee)|%(account)|%(join(note))\n\""
EOF
```

**You will need to customise these** to match your setup. Here's a rundown of
what each of these are for:

* `LME_SYNC_DIR`: full path to the MyExpenses sync backend location. If you've
  not setup sync in MyExpense yet that's ok, but this must be a location that
  will eventually be synced with your mobile devices. It will typically contain
  a dir for each account in MyExpenses, identified by a UUID.
* `LME_ACCOUNT_LABEL`: MyExpenses can manage several 'accounts'. To decide which
  one we'll sync with enter the label here. This label is visible in
  the MyExpenses UI at the top of the main screen or by going to the burger
  menu. Again if the account does not yet exist in MyExpenses it does not matter
  at this stage.
* `LME_LEDGER_START_DATE`: the start date from which the sync will be based.
  This is plugged into ledger to generate reports so should be in ledger date
  format: `YYYY/MM/DD`. The time is always 00:00, i.e. midnight (in the morning)
  of the date given.
* `LME_LEDGER_CMD_OPENING_BAL`: a ledger cmd which gives the opening balance as
  a single numerical value without currency. Multiple lines can be returned by
  this cmd, we just take the final amount printed. Do not include any date
  filtering, we will append the correct begin/end dates to the cmd to limit the
  report. You can quote this string however you choose but be careful of
  escaping any quotes inside the string. This config file is interpreted as pure
  BASH.
* `LME_LEDGER_CMD_TRANSACTIONS`: a ledger cmd which gives the transactions to
  sync with MyExpenses. Again don't add any date filtering to this cmd, we will
  take care of it. The format should be:

```text
...
filename|beg_line|date|amount|payee|account|note
filename|beg_line|date|amount|payee|account|note
...
```

  Each field must be separated by a pipe. The fields names above are the same as
  the expressions you'll need in your ledger format string. See our example
  config above for a start. FYI we need the `filename` and `beg_line` of each
  posting to locate them when updates are applied to the ledger files during
  sync. Again be mindful of quoting, this file will be interpreted as BASH.

  The date, payee, amount is saved so that `lme` can remember what has already been
  synced to MyExpenses. When crafting your ledger query ensure that this
  combination is unique. This is easy if you are pulling transactions from one
  physical account but across multiple it may be a challenge, e.g. you have a
  two subscriptions to the same organisation paid from two accounts on the same
  day. In this case I suggest adding a prefix/suffix to the payee to help `lme`
  distinguish, such as the name of the file/bank account the transaction came
  from.

  The order should be such that line number per filename is always increasing, due
  to assumptions in the code when we update ledger files later (if we add a line,
  we a 1 to the line offset for all subsequent file changes). I think this would
  almost always be the case as new transactions are usually appended to files but
  there might be good reasons this assumption is wrong. This could be fixed so we
  don't care by ordering our changes by line number.

#### 2. Categories

The categories config lets you setup the category tree you'll use in MyExpenses.
It should be located at `$HOME/.config/ledgermyexpenses/categories`. Each
MyExpenses category needs a label and an icon. Categories can have a single (or
no) parent and multiple children. Here's an example config:

```categories
# Comments start with a '#'
# Categories are defined by a line
# Label[,parent...]|icon
#
# Top level categories
#
Bills|plus
Savings|coins
Emergency|user-shield
Income|envelope
#
# Savings sub-categories
#
Car,Savings|car
Pet,Savings|paw
Charity,Savings|ribbon
Clothes,Savings|shirt
Entertainment,Savings|dice
Gifts,Savings|gift
Groceries,Savings|basket-shopping
Health,Savings|suitcase-medical
Holiday,Savings|route
Home,Savings|house
Subscriptions,Savings|film
Taxis,Savings|taxi
```

Commented lines start with a `#` and are ignored. To define a category use the
line:

```config
LABEL[,PARENT]|ICON
```

e.g.

```categories
Bills|plus
```

This defines a new top-level category called 'Bills' with the icon `plus`. The
`plus` is the icon name for your chosen icon from the
[fontawesome](https://fontawesome.com/search) free collection.

To define a sub-category you need to specify it's parent after a comma:

```categories
Car,Savings|car
```

So this creates `Car` under `Savings` with the icon `car`.

#### 3. Mappings

The mappings config turns ledger `accounts` into MyExpenses' categories:
It's located at `$HOME/.config/ledgermyexpenses/mappings`. Here's an example:

```config
# Ledger Account|Category
Assets:Bills|Bills
Assets:Income|Income
Assets:Emergency|Emergency
Assets:Savings:Car|Car,Savings
Assets:Savings:Pet|Pet,Savings
Assets:Savings:Charity|Charity,Savings
Assets:Savings:Clothes|Clothes,Savings
Assets:Savings:Entertainment|Entertainment,Savings
Assets:Savings:Gifts|Gifts,Savings
Assets:Savings:Groceries|Groceries,Savings
Assets:Savings:Health|Health,Savings
Assets:Savings:Holiday|Holiday,Savings
Assets:Savings:Home|Home,Savings
Assets:Savings:Subscriptions|Subscriptions,Savings
Assets:Savings:Taxis|Taxis,Savings
```

Again comment lines start with a `#` are ignored. Each line should map a
ledger account to a MyExpenses category:

```mappings
ACCOUNT|CATEGORY
```

So for example:

```mappings
Assets:Savings:Car|Car,Savings
```

Maps ledger's account `Assets:Savings:Car` to the MyExpenses category `Car`
underneath the `Savings` category.

If any mappings are missing the `lme` will abort and print a message.

#### 4. Mappings - Ledger

Finally we must map MyExpenses categories back to ledger accounts. This config
is located at `$HOME/.config/ledgermyexpenses/mappings_ledger`. This might seem
like a duplicate/reverse of the previous config but there may be circumstances
under which the two configs might differ (maybe). It's a similar format:

```mappings
# CATEGORY|LEDGER ACCOUNT 1|LEDGER ACCOUNT 2
Car,Savings|Assets:Savings:Car|Expenses:Car
Pet,Savings|Assets:Savings:Pet|Expenses:Pet
Charity,Savings|Assets:Savings:Charity|Expenses:Charity
Clothes,Savings|Assets:Savings:Clothes|Expenses:Clothes
Entertainment,Savings|Assets:Savings:Entertainment|Expenses:Entertainment
Gifts,Savings|Assets:Savings:Gifts|Expenses:Gifts
Groceries,Savings|Assets:Savings:Groceries|Expenses:Groceries
Health,Savings|Assets:Savings:Health|Expenses:Health
Holiday,Savings|Assets:Savings:Holiday|Expenses:Holiday
Home,Savings|Assets:Savings:Home|Expenses:Home
Subscriptions,Savings|Assets:Savings:Subscriptions|Expenses:Subscriptions
Taxis,Savings|Assets:Savings:Taxis|Expenses:Taxis
```

This time we take a category and map it back to the two accounts we'll need to
update the ledger transaction:

```mappings
Car,Savings|Assets:Savings:Car|Expenses:Car
```

This maps the MyExpenses category `Car` (underneath `Savings`) to ledger
accounts `Assets:Savings:Car` and `Expenses:Car`. So if we mark a transaction
with this category the transaction in ledger will be updated like this:

```ledger
YYYY/MM/DD * PAYEE
  Assets:Savings:Car       £99
  Expenses:Car
```

### Configuration of MyExpenses

There's no simple way to import a new account from a sync backend in MyExpenses
unfortunately. Instead we must either create the account in MyExpenses
ourselves or reset the app completely so it imports accounts from the sync
backend. Below we detail these choices and the trade-offs:

* **Create/use existing account in MyExpenses for `lme` to sync against** - this
  is a much simpler method but you need to enter the right starting balance
  manually (we may fix this in `lme` so it's not necessary in future).
  * Make sure you've setup synchronization in MyExpenses already in
    Settings-Features-Synchronization-Synchronization backends. The location you
    specify must be reachable by the device where you're running `lme` (the
    `LME_SYNC_DIR` setting).
  * Create a new account in the app. Be sure to use the same name you used in
    setting `LME_ACCOUNT_LABEL`. Be sure to select the right synchronization
    option under 'Synchronization".
  * To get the correct starting balance you can run your
    command from the `lme` config (see setting `LME_LEDGER_CMD_OPENING_BAL`).
    You'll need to add an end date to that cmd `-e DATE`. The end date should be
    the same as your stating date for `lme` (see setting
    `LME_LEDGER_START_DATE`).
  * Ensure MyExpenses has synced the account and the new account dir and files
    have appeared on the device where you are going to run `lme`. Check the path
    you have configured for `lme` (the `LME_SYNC_DIR` setting).
  * Run `lme`. It should now add more files to the account in the sync dir.
  * Back in MyExpenses hit "Sync now" in the account. After a delay (could be a
    few seconds depending on the number of transactions) the new transactions
    should appear.
* **Let LedgerMyExpenses set up the new account for MyExpenses** - but you need to
  completely reset the MyExpenses app to import it (is there a better way?). Not
  good if already have the app configured how you like it and the re-import
  process is a bit wonky. But you do avoid having to manually set an opening
  balance in MyExpenses. Steps:
  * Run `lme` ensuring you use a new account name that does not exist in
    MyExpenses (setting `LME_ACCOUNT_LABEL`). In the sync dir (`LME_SYNC_DIR`
    setting) you'll see a new dir created and the initial transactions created
    as JSON files.
  * Reset the MyExpenses app: delete all data through Android app management
    (App info-Storage and cache-Clear storage).
  * Start the MyExpenses app. During initial setup instead of setting up a first
    account instruct it to "Restore from cloud synchronization". This option is
    hidden under the 'person' icon at the bottom left.

    ![Screenshot of 'Let us set up your first account' screen](docs/Screenshot_20230620-135510.png)

    Ignore any existing sync options that might already be in the menu, you have
    to set sync up from scratch in order to grant the right permissions to the
    app again. So choose one of "SAF", Dropbox or WebDAV and choose the right
    location (same location you have configured for LedgerMyExpenses, the
    `LME_SYNC_DIR` setting).

    The app will report an error at this point. This seems to be a bug. Instead
    ignore that error and select the 'person' icon again and this time choose
    the sync option you just setup. Now you'll get a new modal menu "Restore
    form cloud sychronization". Choose the Accounts tab and select the account
    label you configured for `lme` (setting `LME_ACCOUNT_LABEL`).

  * The account will apear in MyExpenses but will appear empty. Hit the '...'
    menu and select 'Sync now'. After a delay (could be a few seconds depending
    on the number of transactions) the new transactions should appear.

Finally MyExpenses has some default categories. If you want to only see the
categories you've setup with `lme` then go into Settings-Manage-Categories and
select and delete everything. MyExpense will not delete categories that are in
use so you'll be left with just what you need. However note that this will
affect all accounts in MyExpenses unfortunately.

To share this account with another user or device you can use MyExpenses backup
function and simply restore on the other device. It can be useful to set the
MyExpenses option "Settings-Backup-Store on synchronization backend" to share
this with other devices easily.

### Regular Syncing

From this point on you can run `lme` regularly whenever there is a need to sync.
Just be careful not to touch your ledger files during a sync or save changes
before a run to avoid data loss.

## Troubleshooting

* Please open an issue if you're having problems. Be careful not to
  include any personal financial information though as it will be public.

* `lme` had a problem and now reports `Backend already locked`.

  We don't yet clean up properly after an issue. To try again remove the
  `.lock.txt` file from the sync backend (the `LME_SYNC_DIR` setting) and run
  `lme` again.

* Why are there a bunch of voided transactions dated January 2000 in my account
  in MyExpenses?

  This is how we ensure our categories are all available in
  MyExpenses. Unfortunately unless a category is in use there is no way through
  the sync feature to make MyExpenses aware of it (yet).

* We store the state of the sync between runs. If you need to reset things
  then delete `$HOME/.local/share/ledgermyexpenses/transactionlog`.

## Roadmap

* DONE: Set a starting balance for new accounts in ME based on a ledger cmd.
* DONE: Turn all transactions from a given start date into sync files for ME
* DONE: Load config from a config
* DONE: Allow us to run this repeatedly, idempotently (only create files for new
  postings in ledger from a given date)
* DONE: Sync category changes back to ledger.
* DONE: Load in all categories even if some are not used. Users
  cannot pick a category that has not been used unless they set it up manually
  in the app.
* DONE: Comments should be synced two-way: pulled from ledger and put into
  MyExpenses. Otherwise every time we reset we will not see comments again. Need
  to consider how we handle comments on the transaction versus those on the
  posting.
* Papercuts
  * Let's fix the opening balance quirk so initial setup is easier
  * Having to have your trans. report ordered correctly is probably accident
    waiting to happen, should be trivial to sort it first, though we want to
    retain the order of same day, same amount, same payee. Maybe we only need to
    sort it just before we alter ledger files.
* Bugs from real-world testing:
  * I fixed some comments in ledger but they don't make their way back to ME
  * Similarly updates to accounts I make in Ledger after the transaction has
    been sent to ME do not get synced to ME. Therefore things appear to have no
    category that I have perhaps fixed.
  * Transactions I have split in ledger do not get reflected in ME. If we add
    more postings that add up to the same amount as a previous transaction of
    same date and payee we should consider it the same and update in ME.
  * Both of these require that we know which way to send updates, i.e. there's
    an ordering to whether it was changed in ME or ledger last. Perhaps this
    means a full sync is needed. Most of the time a change will only occur on
    one side or the other. When it occurs in both maybe user has to choose. Do
    we need a UI? If we do proper syncing let's start using TDD as there'll be
    devils in the details.
* Find a way to run it as a regular sync job
  * Systemd svc triggered on file changes in a dir would be ideal.
  * We will change the files ourself, so don't want to be continually
    triggered.
  * We need to observe both the ledger files and the backend files
* Commit to git if ledger files are in a repo
* Adjust opening bal? We can't see from what date it starts, so no way to
  verify/know if it's right until/unless we match up every existing trans.
  * Possibly this should be an option for first sync, you only really want to do
    it once I think. Though I suppose we could just check it each time and
    update if needed.
  * What happens when we change opening balance on app? It updates metadata.json
    and pops a new json file in:

    ```json
    [
      {
        "type": "metadata",
        "uuid": "_ignored_",
        "timeStamp": 1686573585
      }
    ]
    ```

* Milestone 'proper sync'
  * We are reading the whole JSON several times, eliminate this.
  * Read and track all the backend JSON data, sync on each run latest files.
    * Record trans
    * Record categories
  * Store the JSON files read against each entry so we can debug: uuid, date,
    amount, payee, order/index
  * Compare the ledger report against this and align the two based on users'
    pref (one-way sync, two-way etc).
  * Ledger report may need to include comments and possibly the two sides of
    each transaction so we can avoid updates to ledger files if not necessary.

Later:

* Support split transactions - categorise parts of a single transaction.
* Learnt through testing
  * Incidentally I can see clearing a cat off a trans seems to give no update to
    the backend. Possible bug/feature in MyExpenses. When you add a new cat we
    do get an update though. So for now it's just that removed categories will
    be invisible to lme.
* Add verification/checking on all inputs/reads, check that files exist/are sane
  etc.
* Improve existing category handling
  * We load existing ones but only use their uuids, we don't check their icon is
    the same. This could lead to use changing the icon on existing categories?
  * We should look out for categories changing their uuid which might indicate
    a category was deleted and re-added with same name?
* Improve error handling
  * Abort and unlock if something goes wrong
    * If we have written trans then write translog
  * Could we assemble all changesets and only write to backend as final act to
    minimise chance of leaving backend locked
* Correctness and performance
  * Do updates by locking ledger files for duration, to stop other processes
    interfering whilst we write files.
  * Only read latest JSON, track what we saw last
    * We can just update first from locked backend from whatever we saw last
    * Then we know everything WE add after that we are automatically up-to-date
      on due to transacitonlog being written.
    * Then save last thing we saw
    * Can we do same for categories, they are slow to load.
  * When we alter ledger files we are not handling all possible valid ledger
    lines e.g. account names with spaces inside them will break our simple
    regexes.
  * Issues with comments
    * Effective dates are seen as comments and prepended to overall posting
      comment. We trim them off if present, is there a better way?
    * What happens if someone updates a comment in ME on any posting which is
      not the first in a transaction (likely for effectively dated ones)? What
      will the ledger report show if a new comment is inserted by ME there?

Ledger-based improvements:

* Discern the category of ledger trans from every account involved in a posting,
  not just first. This gives greater control/fidelity to categories, where
  perhaps the first/source account is not enough to determine a suitable
  category but the second/target account(s) are. But we'll have to carefully
  interpret the ledger transaction report to ensure we match everything up OK.

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue and PRs will be considered. For full details see
[CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* Source code is licensed under GPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

LedgerMyExpenses is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
